<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Recipe;

class RecipeController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */

     public function index(){

    return $this->render('index.html.twig');
     }
     
   /**
    * @Route("/recipes", name="recipe")
    */
   public function allRecipe()
   {
        $repository = $this->getDoctrine()->getRepository(Recipe::class);
        $recipes = $repository->findAll();

       return $this->render('recipes/list.html.twig',
        ["recipes" => $recipes]);
   }
    
   /**
    * @Route("/recipe/{id}", name="recipe_show")
    */
    public function show($id){

    $repository = $this->getDoctrine()->getRepository(Recipe::class);
    $recipe = $repository->find($id);
    return $this->render('recipes/details.html.twig',
    ['recipe' => $recipe]);
    }

}
